package com.example.javacrypto;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;
import sun.security.jca.Providers;

import java.security.SecureRandom;

@SpringBootApplication
public class A_SecureRandom_Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(A_SecureRandom_Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        if (false) {

            System.out.println("\n\n\n\n\n");
            System.out.println("Secure Random Class");


            SecureRandom secureRandom = new SecureRandom();
            System.out.println(secureRandom.getProvider().getInfo());
            System.out.println(secureRandom.getAlgorithm());

            /**
             * Generate without seed
             * Number always generate new
             * Seed is randomized
             */
            byte[] randBytes = new byte[10];
            secureRandom.nextBytes(randBytes);
            System.out.println("\n without seed");
            for (byte b : randBytes) {
                System.out.print(b + "; ");
            }

            System.out.println("\n");

            /**
             * Generate with seed
             * Seed is equal a static position to random/genearte number from that position
             * When seed is static or set, it will always generate the same value
             */
            SecureRandom secureRandom2 = new SecureRandom();
            byte[] randBytes2 = new byte[10];
            secureRandom2.setSeed(1);
            secureRandom2.nextBytes(randBytes2);
            System.out.println("\n -------with seed---------");
            for (byte b : randBytes2) {
                System.out.print(b + "; ");
            }

            System.out.println("\n");

            /**
             * Auto Generate seed
             */
            SecureRandom secureRandom3 = new SecureRandom();
            byte[] seeds = secureRandom3.generateSeed(5);
            System.out.println("\n -------show auto generate seed---------");
            display(seeds);
            System.out.println("");
            secureRandom3.setSeed(seeds);
            byte[] randBytes3 = new byte[10];
            secureRandom3.nextBytes(randBytes3);

            System.out.println("\n -------auto generate seed---------");
            display(randBytes3);

            System.out.println("\n");

            /**
             * non auto Generate seed
             */
            SecureRandom secureRandom4 = new SecureRandom();
            byte[] seeds2 = {1, 2, 3, 4, 5};
            System.out.println("\n -------show static seed---------");
            display(seeds2);
            System.out.println("");
            secureRandom4.setSeed(seeds2);
            byte[] randBytes4 = new byte[10];
            secureRandom4.nextBytes(randBytes4);

            System.out.println("\n -------non auto generate seed---------");
            display(randBytes4);

            System.out.println("\n");

            /**
             * Strong Algorithm
             */
            SecureRandom secureRandom5 = SecureRandom.getInstanceStrong();
            Providers.getFullProviderList().providers().forEach(p -> System.out.println(p.getName()));
            byte[] randBytes5 = new byte[5];
            secureRandom5.nextBytes(randBytes5);
            System.out.println("\n -------with strong algorithm---------");
            display(randBytes5);

            System.out.println("\n");


            /**
             * With Providers
             */
            SecureRandom secureRandom6 = SecureRandom.getInstance("SHA1PRNG", Providers.getSunProvider());
            byte[] randBytes6 = new byte[10];
            secureRandom6.setSeed(1);
            secureRandom6.nextBytes(randBytes6);
            System.out.println("\n -------with provider---------");
            display(randBytes6);

        }

    }

    void display(byte[] randBytes) {
        for (byte b: randBytes) {
            System.out.print(b + "; ");
        }
    }
}
