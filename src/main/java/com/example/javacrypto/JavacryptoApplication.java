package com.example.javacrypto;

import com.sun.org.apache.xml.internal.security.keys.KeyUtils;
import org.apache.logging.log4j.message.Message;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.annotation.Order;
import sun.security.tools.KeyStoreUtil;
import sun.security.util.KeyUtil;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.net.Socket;
import java.security.Identity;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreSpi;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignedObject;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.AlgorithmParameterSpec;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class JavacryptoApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JavacryptoApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {

        if (false) {
            Map<String, String> database = new HashMap<>();
            String credential = register("Hello World!");
            database.put("john", credential);

            String clientName = "john";
            String clientPassword = database.get(clientName);
            String timestamp = Timestamp.from(Instant.now()).toString();


            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(clientName.getBytes());
            md.update(clientPassword.getBytes());
            md.update(timestamp.getBytes());
            String clientDigest = Base64.getEncoder().encodeToString(md.digest());

            MessageDigest mdServer = MessageDigest.getInstance("MD5");
            mdServer.update(clientName.getBytes());
            mdServer.update(credential.getBytes());
            mdServer.update(timestamp.getBytes());
            String serverDigest = Base64.getEncoder().encodeToString(mdServer.digest());

            System.out.println("Message Digest MD5");
            System.out.println("Credential: " + credential);
            System.out.println("Client Digest: " + clientDigest);
            System.out.println("Server Digest: " + serverDigest);
            System.out.println("Verified: " + MessageDigest.isEqual(clientDigest.getBytes(), serverDigest.getBytes()));
            System.out.println("================================================");

            System.out.println("MAC");
            String inputData = "Hello World!";
            String key = "mysecret";
            SecretKey secretKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secretKey);
            mac.update(inputData.getBytes());
            byte[] m = mac.doFinal();
            System.out.println("MAC: " + Arrays.toString(m));
            System.out.println("MAC: " + Base64.getEncoder().encodeToString(m));
            System.out.println("==========================================");

            System.out.println("Signature");
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
            keyGen.initialize(1024);
            KeyPair pair = keyGen.generateKeyPair();
            PrivateKey privateKey = pair.getPrivate();
            PublicKey publicKey = pair.getPublic();

            System.out.println("PrivateKey: " + Base64.getEncoder().encodeToString(privateKey.getEncoded()));
            System.out.println("PublicKey: " + Base64.getEncoder().encodeToString(publicKey.getEncoded()));


            Signature signatureSign = Signature.getInstance("DSA");
            signatureSign.initSign(privateKey);
            signatureSign.update(inputData.getBytes());
            byte[] sign = signatureSign.sign();
            String signEncode = Base64.getEncoder().encodeToString(sign);

            System.out.println("Signature sign: " + signEncode);


            Signature signatureVerify = Signature.getInstance("DSA");
            signatureVerify.initVerify(publicKey);
            signatureVerify.update(inputData.getBytes());
            byte[] signature = Base64.getDecoder().decode(signEncode);
            boolean verify = signatureVerify.verify(signature);

            System.out.println("Signature verify: " + verify);
            System.out.println("===================================");


            System.out.println("SignedObject");
            SignedObject signedObject = new SignedObject(inputData, privateKey, Signature.getInstance("DSA"));
            byte[] objSign = signedObject.getSignature();
            System.out.println("Sign: " + Base64.getEncoder().encodeToString(objSign));

            boolean verifyPublicKey = signedObject.verify(publicKey, Signature.getInstance("DSA"));
            System.out.println("Verify: " + verifyPublicKey);
            System.out.println("================================================");

//        Certificate

//        CertificateFactory cf = CertificateFactory.getInstance("DSA");
//        Certificate c = cf.generateCertificate(new ByteArrayInputStream(publicKey.getEncoded()));
//        c.verify(publicKey);


            Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
            SecretKey secretKey2 = new SecretKeySpec(key.getBytes(), "DES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey2);
//        byte[] raw = cipher.update(inputData.getBytes());
            Map<String, String> data = new HashMap<String, String>() {{
                put("data", inputData);
            }};
            byte[] raw = cipher.doFinal(data.toString().getBytes());

            System.out.println(Base64.getEncoder().encodeToString(raw));

//         data =new HashMap<String, String >(){{
//            put("data", inputData);
//        }}; raw = data.toString().getBytes();
//        raw = Base64.getDecoder().decode("SGdJqkSgX1Vdbfexks95kQ5Kox0pDsfk".getBytes());

            Cipher cipher2 = Cipher.getInstance("DES/ECB/PKCS5Padding");
            cipher2.init(Cipher.DECRYPT_MODE, secretKey2);
//        byte[] raw2 = cipher.update(raw);
            byte[] raw2 = cipher2.doFinal(raw);
            System.out.println(new String(raw2));
        }
    }


    public String register(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        return Base64.getEncoder().encodeToString(digest);
    }
}
