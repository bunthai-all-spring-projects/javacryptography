package com.example.javacrypto;

import ch.qos.logback.core.encoder.ByteArrayUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import sun.security.util.Pem;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;

@SpringBootApplication
public class B_Cipher_Application implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(B_Cipher_Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (true) {

            System.out.println("\n\n\n\n\n");
            System.out.println("=======SHA256withRSA==========");
            /**
             * with SHA256withRSA provider
             */


            // Sender
            KeyPair keyPair = KeyPairGenerator.getInstance("RSA", "SunRsaSign").genKeyPair();

            Files.write(Paths.get("keypair/privateKey.txt"), Base64.getEncoder().encode(keyPair.getPrivate().getEncoded()), StandardOpenOption.TRUNCATE_EXISTING);


            Signature signature = Signature.getInstance("SHA256withRSA", "SunRsaSign");
            PrivateKey privateKey = keyPair.getPrivate();
            signature.initSign(privateKey);
            String data = "Hello";
            signature.update(data.getBytes());
            byte[] sign = signature.sign();
            System.out.println(sign);

            // Receiver
            Signature signature2 = Signature.getInstance("SHA256withRSA", "SunRsaSign");
            PublicKey publicKey = keyPair.getPublic();
            signature2.initVerify(publicKey);
            signature2.update(data.getBytes());
            boolean verify = signature2.verify(sign);
            System.out.println(verify);





        }

    }
}
